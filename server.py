import socket

def main():
    host = '127.0.0.1'
    port = 12345

    # สร้าง socket object
    server_socket = socket.socket()
    
    # กำหนดให้ socket ของ server bind กับ host และ port
    server_socket.bind((host, port))

    # สั่งให้ server รอการเชื่อมต่อจาก client
    server_socket.listen(1)
    print("Waiting for a connection...")

    # ทำการ accept connection จาก client
    connection, address = server_socket.accept()
    print("Connected to:", address)

    num = 1
    while num <= 100:
        # ส่งค่าตัวเลขไปที่ client
        connection.send(str(num).encode())

        # รับค่าตัวเลขที่ client ส่งกลับมา
        received_num = connection.recv(1024).decode()
        num = int(received_num) + 1

        # print ค่าที่ server ได้รับ
        print("Received from client:", received_num)
    
    # ปิดการเชื่อมต่อและ socket
    connection.close()
    server_socket.close()

if __name__ == "__main__":
    main()