import socket

def main():
    host = '127.0.0.1'
    port = 12345

    # สร้าง socket object
    client_socket = socket.socket()

    # ทำการเชื่อมต่อกับ server
    client_socket.connect((host, port))

    while True:
        # รับค่าที่ server ส่งมา
        received_num = client_socket.recv(1024).decode()

        if not received_num:
            break

        # print ค่าที่ client ได้รับ
        print("Received from server:", received_num)

        # ส่งค่าที่ได้รับมา +1 กลับไปที่ server
        client_socket.send(str(int(received_num) + 1).encode())

    # ปิดการเชื่อมต่อและ socket
    client_socket.close()

if __name__ == "__main__":
    main()